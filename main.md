# Suomalaisen Linux-käyttäjän pakolliset verkkosivut
## [linux.fi](https://www.linux.fi/wiki/Etusivu)
Suomenkielinen Linux-wiki ja foorumi. Hyvä tietolähde kaikissa perusasioissa.
